use rayon::iter::ParallelBridge;
use rayon::iter::ParallelIterator;
use std::collections::HashSet;
use std::error::Error;
use std::fs::{create_dir_all, read_dir, File};
use std::io::{BufRead, BufReader, Read, Write};
use std::path::Path;

////////////////////////////////////////////
const OUTPUT_DIR: &str = "./outputs";
const INPUT_DIR: &str = "./inputs";
const DIC: &str = "./dic.txt";
////////////////////////////////////////////

fn process_txt(txt: impl Into<String>) -> Result<String, Box<dyn Error>> {
    let txt = txt.into();
    let re = regex::Regex::new(r"[^\w]")?;
    let new_txt = re.replace_all(&txt, "");
    Ok(new_txt.to_lowercase())
}

fn make_dic() -> Result<HashSet<String>, Box<dyn Error>> {
    let mut dic = HashSet::new();
    let dic_file = File::open(DIC)?;
    for line in BufReader::new(dic_file).lines() {
        if let Ok(word) = line {
            dic.insert(String::from(word.trim()));
        }
    }
    Ok(dic)
}

fn process_input(
    infile_path: impl AsRef<Path>,
    dic: &HashSet<String>,
) -> Result<(), Box<dyn Error>> {
    let mut file = File::open(&infile_path)?;
    let mut file_text = String::new();
    let _ = file.read_to_string(&mut file_text)?;
    let text = process_txt(file_text)?;
    let mut all_possible_words = HashSet::new();
    for i in 1..text.len() {
        for j in 0..text.len() {
            let word: String = text.chars().skip(j).take(i).collect();
            all_possible_words.insert(word);
        }
    }
    let mut results = vec![];
    for word in all_possible_words {
        if dic.contains(&word) {
            results.push(word);
        }
    }
    write_output(&infile_path, results)?;
    Ok(())
}

fn write_output(infile_path: impl AsRef<Path>, results: Vec<String>) -> Result<(), Box<dyn Error>> {
    let infile_path = infile_path.as_ref();
    create_dir_all(OUTPUT_DIR)?;
    let mut outfile = File::create(format!(
        "{}/{}.out.txt",
        OUTPUT_DIR,
        infile_path.file_name().unwrap().to_str().unwrap()
    ))?;
    for word in results {
        outfile.write_all(format!("{}\n", word).as_bytes())?;
    }
    outfile.sync_all()?;
    Ok(())
}

fn main() {
    let dic = make_dic().expect("make dic error");
    read_dir(INPUT_DIR)
        .expect("Read Dir Error")
        .par_bridge()
        .for_each(|path| {
            if let Ok(path) = path {
                let _ = process_input(path.path(), &dic);
            }
        });
}
